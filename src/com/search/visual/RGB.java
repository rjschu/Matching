package com.search.visual;

public class RGB {
	

	
	
	private int R;
	private int G;
	private int B;
	
	RGB(){R=0; G=0; B=0; }
	RGB(int R, int G, int B){this.R=R; this.G=G; this.B=B;}
	
	
	public int getR(){ return R; }
	public int getG(){ return G; }
	public int getB(){ return B; }
	
	public void setR(int r) {
		R = r;
	}
	public void setG(int g) {
		G = g;
	}
	public void setB(int b) {
		B = b;
	}
	
	
	

}
