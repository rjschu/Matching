package com.search.visual;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;


public class ImageChunks {
	
  //start variable declaration	
	private RGB[][] rgbList = null;
	private int rows = 0;
	private int cols = 0;
	private BufferedImage bmp = null;
	private Rectangle rec = new Rectangle(80, 80);
  //end variable declaration
	
	
	
	
	
	ImageChunks(BufferedImage im1, int rows, int cols)
	{
		/* assign values */
		
		  this.bmp = im1;
		
		
	
		
		this.rows = 3; //rows;
		this.cols = 3; //cols;
		rgbList = new RGB[rows][cols];//creates new RGB multiple Array and passes rows and columns through as sizes 
		
				
		calculateChunks();		
	}
	
	
	
	/*
	 *   getRGBList
	 * 
	 */
	
	public RGB[][] getRGBList()
	{
		return rgbList;
	}
	
	/*
	 *creates new image by cropping original image based on the specified x,y co-ordinates
	 *and based on the rectangle size
	 */
	
	 private BufferedImage cropImage(BufferedImage src, Rectangle rect, int x, int y) {
	      BufferedImage dest = src.getSubimage(x, y, rect.width, rect.height);
	      return dest; 
	   }
	
	/*
	 * divides image image into 9 sections. 
	 */
	
	private BufferedImage[][]  createImageArrays()
	{
		BufferedImage[][] bitmapsArray = new BufferedImage[3][3];
	   
		bitmapsArray[0][0] = cropImage(bmp, rec, 0, 0);
		bitmapsArray[0][1] = cropImage(bmp, rec, 80, 0);
		bitmapsArray[0][2] = cropImage(bmp, rec, 160, 0);
		bitmapsArray[1][0] = cropImage(bmp, rec, 0, 80);
		bitmapsArray[1][1] = cropImage(bmp, rec, 80, 80);
		bitmapsArray[1][2] = cropImage(bmp, rec, 160, 80);
		bitmapsArray[2][0] = cropImage(bmp, rec, 0, 160);
		bitmapsArray[2][1] = cropImage(bmp, rec, 80, 160);
		bitmapsArray[2][2] = cropImage(bmp, rec, 160, 160);
		
		

	    return bitmapsArray;
	    
	    
	}

	
	

	private void calculateChunks()
	{
		
		
		BufferedImage[][] bmpList = createImageArrays(); //creates new bufferedimage array based on returned results from method
		
		BufferedImage bmp_temp = null; 
		
		int red = 0;
		int green = 0;
		int blue = 0;
		
		int running_total_red = 0;
		int running_total_blue = 0;
		int running_total_green = 0;
		
		RGB temp_rgb = null;
		
		for(int i=0; i < 3; i++)
			for(int j=0; j < 3; j++)
			{//loops through 3 times in both for loops to represent each row and its columns
				
				bmp_temp = bmpList[i][j];//passes the sections of image into bmp_temp 
				
				red = 0;
				green = 0;
				blue = 0;
				
				for(int k=0; k < 80; k++)
					for(int l=0; l < 80; l++)
					{//loops through 80 times so it goes through each pixel in the image
						
						temp_rgb = getPixelRGBvalues( bmp_temp.getRGB(k, l) );//calls getPixelRGBvalues method and passes in the rgb values of the current pixels in the image
						
						//gets current value of each bin and adds it to the total
						running_total_red += temp_rgb.getR();
						running_total_blue += temp_rgb.getB();
						running_total_green += temp_rgb.getG();
					
					
					}
				
				
								
				// normalise these values				
				red 	= running_total_red/ (80*80);
				green 	= running_total_green/ (80*80);
				blue 	= running_total_blue/ (80*80);
							
				
				rgbList[i][j] = new RGB(red, green, blue);//creates new instance of rgb in each node of the array
			}
	}
	
	
	

	private RGB getPixelRGBvalues( int pixel )
	{ 
		
		  int clr = pixel; 
		  //picks each color out of the pixel and stores value to variable
		  int  red   = (clr & 0x00ff0000) >> 16;
		  int  green = (clr & 0x0000ff00) >> 8;
		  int  blue  =  clr & 0x000000ff;
		 
     
		
		return new RGB(red,green,blue); 
		
	}	

	
	
	public int compareImage(RGB[][] rgb_image_list)
	{
		// one by one we will get Euclidean distance of RGB image from each [][]
		// normalise by dividing by rows * cols for average distance

		int total_distance = 0;
		
		for(int r=0; r < rows; r++)
			for(int c=0; c < cols; c++)
			{
				total_distance += EuclideanDistance(rgbList[r][c],rgb_image_list[r][c]);
			}
		
		return total_distance/(3*3);
	}
	
	
	public int EuclideanDistance( RGB rgb1, RGB rgb2 )
	{
		int distance = 0;
		//gets the value of query colour value - other image colour value and multiplies it by itself 
		int red = (int) Math.pow(rgb1.getR()-rgb2.getR(),2);
		int green = (int) Math.pow(rgb1.getG()-rgb2.getG(),2);
		int blue = (int) Math.pow(rgb1.getB()-rgb2.getB(),2);
		distance =  (int) Math.sqrt( red+green+blue);//gets square root of all values added together
        distance = (int)(distance / 4.43);//normalises by dividing my 4.43
        
		return distance;
	}
	
	
}
