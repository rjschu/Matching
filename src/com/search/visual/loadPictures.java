package com.search.visual;

import java.io.File;
import java.util.ArrayList;

public class loadPictures {

	//declare variables
		private File[] files;
		private String fnames;
		private ArrayList<File> ifiles = new ArrayList<File>();
		private File dir;
	//end declaration
		
		public loadPictures(String dir){
			this.dir = new File(dir);
		}
		
		public ArrayList<File> getFiles(){
			
			files = dir.listFiles(); //loads all files from the dir variable into files
			
			for(int i =0; i<files.length; i++){
				/*
				 * loops through each file in the chosen directory and checks to see if it is a image file and if so adds to the ifiles ArrayList
				 */
				if(files[i].isFile()){
					fnames = files[i].getName();
					if(fnames.endsWith("jpg") || fnames.endsWith("JPG")|| fnames.endsWith("png")|| fnames.endsWith("PNG")|| fnames.endsWith("bmp")|| fnames.endsWith("BMP")){
						
						
						ifiles.add(new File(fnames));
					}
				}
			
			}
			return ifiles;
			
		}
		

		
}
