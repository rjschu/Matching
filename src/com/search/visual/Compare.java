package com.search.visual;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

public class Compare {
	
	//start variable declaration
	private TreeMap<Integer, String> val = new TreeMap<Integer, String>();
	private List<SortedMap<Integer, String>> vals = new LinkedList<SortedMap<Integer,String>>();
	//end variable declaration
	
	
	
	public void startCompare(String file, ArrayList<File> names, File query){
		
		
			
			BufferedImage ima= null;
			try {
				ima = ImageIO.read(query);// creates bufferedimage
				if(ima.getHeight() <240 || ima.getWidth() < 240){
					//checks if images height and width is below 240 if not go back to choice class 
					JOptionPane.showMessageDialog(null, "Please choose an image with a Width and Height of 240px or more");
					new choice();
					return;//stops method execution continuing 
				}
				
			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (Throwable e) {
				e.printStackTrace();
			}
			ImageChunks im = new ImageChunks(ima,3,3);//creates instance of ImageChunks Class and passes in query image and two ints
			
			
			for(int j = 0; j<names.size(); j++){
			/*
			 * loops through each file and checks images are again not less then 240px 
			 * if the images are it skips past that image.
			 */
							
				BufferedImage ima1 = null;
				try {
					ima1 = ImageIO.read(new File(file+"/"+names.get(j)));
					if(ima1.getHeight() < 240 || ima1.getWidth()<240){
						continue;
						
					}
				} catch (IOException e) {
                    System.out.println(names.get(j));
					//e.printStackTrace();
                    continue;
				}
		
				ImageChunks im1 = new ImageChunks(ima1,3,3);
				
				int Distance = im.compareImage(im1.getRGBList());//calls compareImage method and passes a method call from the im1 instance of Imagechunks
				
				if(Distance < 100){
					//adds file to val and then adds it to vals list
					val.put(Distance,file+"/"+names.get(j));
					vals.add(0,val);			
				}
		
			
			
		}
			
			new Results(vals);//creates new instance of Results and passes vals through
			
	}
}
		
		
		
