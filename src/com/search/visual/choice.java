package com.search.visual;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

public class choice {
	
		private JFrame frame = new JFrame("Image Matching");
		private JLabel info = new JLabel("Please Choose a query image");
		private JButton fichoice = new JButton("Choose a query image");
		private JLabel info2 = new JLabel("Please Choose a Directory to compare images to");
		private JButton dirchoice = new JButton("Choose a directory");
		private File query;
		private final JFileChooser chooser = new JFileChooser();
		private GridBagConstraints gbc = new GridBagConstraints();
		private JPanel jp = new JPanel();
		private resizeImage r = new resizeImage();
		private BufferedImage im = null;
		private Container cont;
		private JLabel qi = null;
		
		private JLabel info1 = new JLabel("your query image is:");
		
		
		public choice(){
			showGui();
		}
		
		
		public Container addComponents(Container con){
			
			cont = con;
			cont.setLayout(new GridBagLayout());
			
			
	        
			gbc.gridx = 1;
			gbc.gridy = 2;
			cont.add(info, gbc);
			
			gbc.gridy = 3;
			fichoice.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					// TODO Auto-generated method stub
					chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);//allows only one file to be selected
					
					
					 int returnVal = chooser.showOpenDialog(null);//gets value of what button is pressed when file chooser is opened
					 
					 if (returnVal == JFileChooser.APPROVE_OPTION) {
				           query = chooser.getSelectedFile();//stored the selected file in query
				           cont = build(cont, query);//calls the build method to show the query image
				           
				        } else {
				        }
				}
			});
			cont.add(fichoice, gbc);
			
			gbc.gridx = 1;
			gbc.gridy = 4;
			cont.add(info2, gbc);
			
			gbc.gridy = 5;
			dirchoice.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					// TODO Auto-generated method stub
					chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);//only allows directories to be selected
					 int returnVal = chooser.showOpenDialog(null);
					
					 if (returnVal == JFileChooser.APPROVE_OPTION) {
						 	String file = chooser.getSelectedFile().toString();//gets selected folder
				            cont.remove(info1);
				            cont.remove(info2);
				            cont.remove(fichoice);
				            cont.remove(dirchoice);
				            frame.pack();
				            loadPictures lp = new loadPictures(file);// creates instance of LoadPictures and passes in the variable file as a paramater
				            ArrayList<File> names = lp.getFiles();// stores returned arraylist from getFiles() in names.
				        	Compare cp = new Compare();
				    		cp.startCompare(file, names, query);//calls startCompare and passes the chosen directory, the arraylist name and the query image as paramater 
				    		
				    		
				        } else {
				        }
				}
			});
			cont.add(dirchoice, gbc);
			
			
			return cont;
		}
		
		public void showGui(){
		

			addComponents(frame.getContentPane());
			frame.pack();
			frame.setVisible(true);
			frame.setLocationRelativeTo(null);
			frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
			frame.setResizable(true);
		}
		
		public Container build(Container cont, File query){
			   
				
			
	   			gbc.gridx = 1;
	   	        gbc.gridy = 0;
	   	        cont.add(info1,gbc);
	   			
	           try {
	        	 if(im != null){
	        		jp.remove(qi);
	        	 }
				im = ImageIO.read(query);
			} catch (IOException e) {
				e.printStackTrace();
			}
	           im = r.resize(im, 640, 480);
	           qi = new JLabel(new ImageIcon(im));
	           jp.add(qi);				           
	           gbc.gridy=1;
	           gbc.gridx = 1;
	           cont.add(jp,gbc);
	           frame.pack();
	           Results r = new Results();
	           r.centre(frame);
			return cont;
		}

		
}
