package com.search.visual;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;

public class Results {
	
	//start variable declaration
	private JFrame frame = new JFrame();
	private GridBagConstraints gbc = new GridBagConstraints();
	private ArrayList<JLabel> images;
	private JLabel info = new JLabel("Results in ascending order");
	private resizeImage r = new resizeImage();
	private Insets ins = new Insets(5, 5, 5, 5);
	private Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
	private JScrollPane jsp;
	private JPanel jp = new JPanel();
	//end variable declaration 
	
	  public Results(List<SortedMap<Integer, String>> vals){
		this.images = createimageDisplays(vals);
		showGui();	
	  }

	  
	  public Results() {
      }


	public void addComponents(Container cont){
		  
		  
		  cont.setLayout(new BorderLayout());
		  jp.setLayout(new GridBagLayout());
		  
			
			
			
			int n = 0;
			int y = 0;
			for(int i = 0; i<images.size(); i++) {	
				
				if(n!= 	4){
					gbc.gridx = n;
					gbc.gridy = y;
				}else{
					gbc.gridx = 0;
					y+=1;
					gbc.gridy = y;
					n=0;
				}
				gbc.gridwidth=1;
				gbc.insets = ins;
				jp.add(images.get(i),gbc);
				n++;
				}
				
			jsp = new JScrollPane(jp);
			cont.add(info,BorderLayout.NORTH);
			cont.add(jsp,BorderLayout.CENTER);
			
			}
				
					
		public void showGui(){
			int w = dim.getSize().width;
			int h = dim.getSize().height;
			frame.setSize(w,h);
			addComponents(frame.getContentPane());
			centre(frame);
			frame.setVisible(true);
			frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
			frame.setResizable(false);
		}
		
		public ArrayList<JLabel> createimageDisplays(List<SortedMap<Integer, String>> vals){
			
			ArrayList<JLabel> images = new ArrayList<JLabel>();
			
			
			/*
			 * for loop is used to loop through each entry in the vals List
			 */
			
			 for(int i =0; i<vals.size(); i++){
				 	
				 	//creates iterator to go through all values in vals
					Iterator<?> it = vals.get(i).entrySet().iterator();
					
				    while (it.hasNext()) {
				        	
						@SuppressWarnings("rawtypes")
						Map.Entry pairs = (Map.Entry)it.next();//gets the pairs from the sorted map in vals
						
				        File f = new File(pairs.getValue().toString());//creates a file out of the value of the pair
				        BufferedImage im = null;
				        try {
							im = ImageIO.read(f);
							im = r.resize(im, 320, 240);
						} catch (IOException e) {
							e.printStackTrace();
						}
				        images.add(new JLabel(new ImageIcon(im)));
				        it.remove(); // avoids a ConcurrentModificationException
				    }
				
			
				 }
			return images;
			
		}
		
		public JFrame centre(JFrame frame){
			int w = frame.getSize().width;
			int h = frame.getSize().height;
			int x = (dim.width-w)/2;//subtracts the frames width from the screen width then divides by 2 
			int y = (dim.height-h)/2;//subtracts the frames height from the screen height and divides by 2
			frame.setLocation(x,y);
			return frame;
		}
		
		
		
		 
}
